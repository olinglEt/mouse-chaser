
    var WIDTH = 10;
    var HEIGHT = 10;
    var colorArray = [
        'blue',
        'green',
        'red',
        'orange',
        'purple'
    ];
    window.addEventListener('mousemove', onMouseMove, false);
    
    /**
     * ßßß
     */
    function onMouseMove(e) {
        if(!document.getElementById('chaser')) {
            document.body.insertAdjacentHTML('beforeend', '<div id="chaser"></div>');
        }
        setRandomLocation(e);
        setRandomSize();
        setRandomColor();
    }
    /**
     * 
     * @param {*} mouseX 
     * @param {*} mouseY 
     */
    function getCenter(mouseX, mouseY) {
        return {
            x: mouseX - WIDTH / 2,
            y: mouseY - HEIGHT / 2
        }
    }

    /**
     * 
     */
    function getRandomOffset() {
        return {
            x: (Math.random() - 0.5) * 100,
            y: (Math.random() - 0.5) * 100
        }
    }
    
    /**
     * 
     * @param {*} e 
     */
    function setRandomLocation(e) {
        var center = getCenter(e.pageX, e.pageY);
        var randomOffset = getRandomOffset();
        document.getElementById('chaser').style.left = (center.x + randomOffset.x) + 'px';
        document.getElementById('chaser').style.top = (center.y + randomOffset.y) + 'px';

    }

    /**
     * 
     */
    function setRandomSize() {
        var radius = 10 + Math.random() * 10;
        document.getElementById('chaser').style.width = radius * 2 + 'px';
        document.getElementById('chaser').style.height = radius * 2 + 'px';
        document.getElementById('chaser').style.borderRadius =  radius + 'px';

    }

    /**
     *
     */
    function setRandomColor() {
        var randInt = Math.floor(Math.random() * 5);
        document.getElementById('chaser').style.background = colorArray[randInt];
    }